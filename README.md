# A Deep-Learning Approach for SAR Tomographic Imaging of Forested Areas
## Zoé Berenger, Loïc Denis, Florence Tupin, Laurent Ferro-Famil, Yue Huang



## Abstract
Synthetic aperture radar tomographic imaging reconstructs the 3-D reflectivity of a scene from a set of coherent acquisitions performed in an interferometric configuration. In forest areas, a high number of elements backscatter the radar signal within each resolution cell. To reconstruct the vertical reflectivity profile, state-of-the-art techniques perform a regularized inversion implemented in the form of iterative minimization algorithms.

We show that lightweight neural networks can be trained to perform this inversion with a single feed-forward pass, leading to fast reconstructions that could better scale to the amount of data provided by the future BIOMASS mission. We train our encoder–decoder network using simulated data and validate our technique on real L-band and P-band data.


## Resources
- [Paper](https://ieeexplore.ieee.org/document/10175623)
- [Preprint (ArXiv)](https://arxiv.org/abs/2301.08605)


To cite the article:
```
Z. Berenger, L. Denis, F. Tupin, L. Ferro-Famil and Y. Huang,
"A Deep-Learning Approach for SAR Tomographic Imaging of Forested Areas"
in IEEE Geoscience and Remote Sensing Letters, vol. 20, pp. 1-5, 2023, Art no. 4007405, doi: 10.1109/LGRS.2023.3293470.

```

## Dependencies
Use the package installer [pip](https://pip.pypa.io/en/stable/) to install recursively dependencies from the file 'requirements.txt'.

```bash
pip install -r requirements.txt
```

## Run examples
The notebook example_BioSAR2_tomo_nn.ipynb show step-by-step how to reconstruct a tomogram with our network.



## Licence
The material is made available under the GNU General Public License v3.0, Copyright (C) 2023, Zoé Berenger, Loïc Denis, Florence Tupin, Laurent Ferro-Famil, Yue Huang




